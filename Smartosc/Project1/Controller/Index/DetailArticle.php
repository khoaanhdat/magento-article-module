<?php

namespace Smartosc\Project1\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;

class DetailArticle extends Action {
	/**
	 * @var PageFactory
	 */
	protected $_pageFactory;

	/**
	 * DetailArticle constructor.
	 * @param Context $context
	 * @param PageFactory $pageFactory
	 */
	public function __construct(
		Context $context,
		PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}


	public function execute()
	{
		return $this->_pageFactory->create();
	}
}