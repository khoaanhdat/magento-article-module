<?php

namespace Smartosc\Project1\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Smartosc\Project1\Helper\Data;

class ListArticle extends Action {

	/**
	 * @var PageFactory
	 */
	protected $_pageFactory;

	/**
	 * @var Data
	 */
	protected $_enableConfigValue;

	/**
	 * ListArticle constructor.
	 * @param Context $context
	 * @param PageFactory $pageFactory
	 * @param Data $enableConfigValue
	 */
	public function __construct(
		Context $context,
		PageFactory $pageFactory,
		Data $enableConfigValue)
	{
		$this->_enableConfigValue = $enableConfigValue;
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		$enable = $this->_enableConfigValue->getGeneralConfig('enable');

		if(1 == $enable){
			return $this->_pageFactory->create();
		}
		else{
			throw new NotFoundException(__('Page Not Found'));
		}

	}
}