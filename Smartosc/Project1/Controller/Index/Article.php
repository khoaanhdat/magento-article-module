<?php
namespace Smartosc\Project1\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Article extends Action
{
	public function __construct(Context $context)
	{
		parent::__construct($context);
	}

	public function execute()
	{
		$article = $this->_objectManager->create('Smartosc\Project1\Model\Article');
		$article->setTitle('Đưa 2 nghi phạm sát hại nam sinh viên chạy Grap ra thực nghiệm hiện trường');
		$article->setContent('Chiều nay 1-10, Công an quận Bắc Từ Liêm (TP Hà Nội) phối hợp cùng các đơn vị nghiệp vụ của Công an TP Hà Nội đã đưa 2 nghi phạm sát hại nam sinh viên Nguyễn Cao S. (SN 2001, trú tại Thanh Hoá) là Đinh Văn Trường (19 tuổi) và Đinh Văn Giáp (24 tuổi, cùng trú tại huyện Văn Chấn, tỉnh Yên Bái) đến hiện trường tại khu phố Cao Phong, phường Thuỵ Phương (quận Bắc Từ Liêm) để thực nghiệm hiện trường.');
		$article->setImage('https://nld.mediacdn.vn/thumb_w/684/2019/10/1/1-1569911670093238799387-15699148190321130336859.png');
		$article->save();
		$this->getResponse()->setBody('success');


	}
}