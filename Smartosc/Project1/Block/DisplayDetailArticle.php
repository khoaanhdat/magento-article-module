<?php
namespace Smartosc\Project1\Block;

use Magento\Framework\View\Element\Template;
use Smartosc\Project1\Model\ResourceModel\Article\CollectionFactory;

class DisplayDetailArticle extends Template
{
	protected $_articleDetail;

	/**
	 * @var CollectionFactory
	 */
	protected $_articleCollectionFactory;

	/**
	 * DisplayDetailArticle constructor.
	 * @param Template\Context $context
	 * @param CollectionFactory $articleCollectionFactory
	 */
	public function __construct(Template\Context $context, CollectionFactory $articleCollectionFactory)
	{
		$this->_articleCollectionFactory = $articleCollectionFactory;
		parent::__construct($context);
	}

	/**
	 * GET A ARTICLE BY ID
	 * @return array
	 */
	public function getArticleById()
	{
		$articleId = $this->getRequest()->getParam('id');
		$this->_articleDetail = $this->_articleCollectionFactory->create()->addFieldToFilter('article_id', $articleId);
		return $this->_articleDetail->getData();
	}
}