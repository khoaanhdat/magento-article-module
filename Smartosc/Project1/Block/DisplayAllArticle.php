<?php

namespace Smartosc\Project1\Block;
use Magento\Framework\View\Element\Template;
use Smartosc\Project1\Helper\Data;
use Smartosc\Project1\Model\ResourceModel\Article\CollectionFactory;
use Smartosc\Project1\Model\ResourceModel\Article\Collection as ArticleCollection;

class DisplayAllArticle extends Template
{
	/**
	 * @var ArticleCollection
	 */
	protected $_articleCollection;

	/**
	 * @var CollectionFactory
	 */
	protected $_articleCollectionFactory;

	/**
	 * @var Data
	 */
	protected $_limitPageConfig;

	/**
	 * DisplayAllArticle constructor.
	 * @param Template\Context $context
	 * @param CollectionFactory $collectionFactory
	 * @param Data $limitPageConfig
	 */
	public function __construct(Template\Context $context,
	                            CollectionFactory $collectionFactory,
								Data $limitPageConfig)
	{
		$this->_limitPageConfig = $limitPageConfig;
		$this->_articleCollectionFactory = $collectionFactory;
		parent::__construct($context);
	}

	public function getAllArticle()
	{
		$this->_articleCollection = $this->_articleCollectionFactory->create()->getData();
		return $this->_articleCollection;
	}

	/**
	 * @param $pageSize
	 * @return array
	 */
	public function getAllArticlePaging($pageSize)
	{
		$pageIndex = $this->getRequest()->getParam('page');

		$this->_articleCollection = $this->_articleCollectionFactory->create();
		if($pageIndex == NULL){
			$pageIndex = 1;
		}

		$article = $this->_articleCollection->getData();
		$start = ($pageIndex - 1) * $pageSize;
		$listArticle = [];
		for ($i = $start, $dem = 1; ($dem <= $pageSize && $i < $this->_articleCollection->count()); ++$dem, ++$i){
			$listArticle[] = $article[$i];
		}
		return $listArticle;
	}

	/**
	 * GET LIMIT PER PAGE CONFIG VALUE
	 *
	 */
	public function getLimitPageConfig()
	{
		return $this->_limitPageConfig->getGeneralConfig('limit_page') + 1;
	}

	/**
	 * GET PAGE CURRENT
	 */
	public function getPageIndex()
	{
		return $this->getRequest()->getParam('page');
	}
}