<?php

namespace Smartosc\Project1\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class ListNumber implements ArrayInterface
{
	/**
	 * Return array of options as value-label pairs
	 *
	 * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
	 */
	public function toOptionArray()
	{
		$arr = $this->toArray(50);
		$res = [];

		foreach ($arr as $key => $value){
			$res[] = [
				'value' => $key,
				'label' => $value
			];
		}
		return $res;
	}


	public function toArray($limit)
	{
		$rs = [];
		for ($i = 1; $i <= $limit; ++$i){
			$rs[] = $i;
		}
		return $rs;
	}
}