<?php
namespace Smartosc\Project1\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Article extends AbstractDb
{
	protected function _construct()
	{
		$this->_init('smartosc_project1_article', 'article_id');
	}
}