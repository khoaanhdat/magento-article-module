<?php
namespace Smartosc\Project1\Model\ResourceModel\Article;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Article Collection
 */
class Collection extends AbstractCollection
{
	/**
	 * Initialize resource collection
	 *
	 * @return void
	 */
	public function _construct() {
		$this->_init('Smartosc\Project1\Model\Article',
			'Smartosc\Project1\Model\ResourceModel\Article');
	}
}