<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
	ComponentRegistrar::MODULE,
	'Smartosc_Project1',
	__DIR__
);