<?php

namespace Smartosc\Project1\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade(SchemaSetupInterface $setup,
	                        ModuleContextInterface $context)
	{
		if (version_compare($context->getVersion(), '2.0.1') < 0) {
			$installer = $setup;
			$installer->startSetup();
			$connection = $installer->getConnection();
			//Install new database table
			$table = $installer->getConnection()->newTable(
				$installer->getTable('smartosc_project1_article')
			)->addColumn(
				'article_id',
				Table::TYPE_INTEGER,
				null, [
				'identity' => true,
				'unsigned' => true,
				'nullable' => false,
				'primary' => true
			],
				'Article Id'
			)->addColumn(
				'title',
				Table::TYPE_TEXT,
				225,
				['nullable' => false],
				'Title'
			)->addColumn(
				'content',
				Table::TYPE_TEXT,
				225,
				['nullable' => false],
				'Content'
			)->addColumn(
				'image',
				Table::TYPE_TEXT,
				225,
				[],
				'Image'
			)->addColumn(
				'create_at',
				Table::TYPE_DATE,
				null,
				[],
				'Create At'
			)->addColumn(
				'update_at',
				Table::TYPE_DATE,
				null,
				[],
				'Update At'
			)->setComment(
				'Article Schedule'
			);
			$installer->getConnection()->createTable($table);
			$installer->endSetup();
		}
	}
}



